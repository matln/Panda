#!/bin/bash
# Copyright 2018   Lantian Li

. ./path.sh
. ./cmd.sh
. ./utils/parse_options.sh

set -e

stage=0
train_stage=-10

use_gpu=true
feat_dim=40
data=fisher_5000
train_data=data/data_fbank/$data 

xvector_dim=200 # dimension of the xVector. configurable.
min_frame_per_chunk=50
max_frame_per_chunk=300
output_period=9
right_context=$((($output_period * $max_frame_per_chunk) + $output_period))

jesus_blocks=1
frames_per_iter=2000000
xvector_dir=exp/xvector_${data}_dim${xvector_dim}_chunk${min_frame_per_chunk}_${max_frame_per_chunk}_block${jesus_blocks}_period${output_period}_right${right_context}_frames_${frames_per_iter}
egs_dir=$xvector_dir/egs


# prepare network configs
if [ $stage -le 0 ]; then
  mkdir -p $xvector_dir/log
  $cmd $xvector_dir/log/make_configs.log \
    steps/nnet3/xvector/make_jesus_configs.py \
      --splice-indexes="-1,0,1 -2,-1,0,1 -3,0,3 -3,0,3 mean(0:3:$output_period:$right_context)" \
      --feat-dim $feat_dim --output-dim $xvector_dim \
      --num-jesus-blocks $jesus_blocks \
      --jesus-input-dim 150 --jesus-output-dim 500 --jesus-hidden-dim 1000 \
      $xvector_dir/nnet.config
fi


# dump egs.
if [ $stage -le 1 ]; then
  steps/nnet3/xvector/get_egs_sre_vad.sh --cmd "$train_cmd" \
    --nj 8 \
    --stage 0 \
    --frames-per-iter $frames_per_iter \
    --frames-per-iter-diagnostic $frames_per_iter \
    --min-frames-per-chunk $min_frame_per_chunk \
    --max-frames-per-chunk $max_frame_per_chunk \
    --num-diagnostic-archives 1 \
    --num-repeats 10 \
    "$train_data" $egs_dir
fi


# model training.
if [ $stage -le 2 ]; then
  # training for 4 epochs * 3 shifts means we see each eg 12
  # times (3 different frame-shifts of the same eg are counted as different).
  steps/nnet3/xvector/train.sh --cmd "$train_cmd" --stage $train_stage \
      --gpu-cmd "$gpu_cmd" \
      --initial-effective-lrate 0.001 \
      --final-effective-lrate 0.0001 \
      --max-param-change 0.2 \
      --minibatch-size 64 \
      --num-epochs 8 --num-shifts 3 --use-gpu $use_gpu \
      --num-jobs-initial 2 --num-jobs-final 8 \
      --egs-dir $egs_dir \
      $xvector_dir
fi


# remove egs
if [ $stage -le 3 ]; then
  echo remove egs.
  # uncomment the following line to have it remove the egs when you are done.
  # steps/nnet2/remove_egs.sh $xvector_dir/egs
fi


# x-vector decoding.
if [ $stage -le 4 ]; then
  period=100    #  configurable.
  sid/extract_xvectors.sh --nj 20 --cmd "$train_cmd" --period $period \
    $xvector_dir data/data_fbank/fisher_test/train_1000
  sid/extract_xvectors.sh --nj 20 --cmd "$train_cmd" --period $period \
    $xvector_dir data/data_fbank/fisher_test/eva_1000
fi


# evaluation scoring
if [ $stage -le 5 ]; then
  echo Scoring 4-4.
  trl=data/trials/4-4.trl
  nnet3-xvector-scoring $xvector_dir/final.raw $trl \
    scp:$xvector_dir/eva_1000/xvector.scp scp:$xvector_dir/eva_1000/xvector.scp $xvector_dir/foo_4_4
  echo
  printf '% 12s' 'EER:'
  eer=$(awk '{print $3}' $xvector_dir/foo_4_4 | paste - $trl | awk '{print $1, $4}' | compute-eer - 2>/dev/null)
  printf '% 7.2f' $eer
  echo

  echo Scoring 40-4.
  trl=data/trials/40-4.trl
  nnet3-xvector-scoring $xvector_dir/final.raw $trl \
    scp:$xvector_dir/train_1000/xvector.scp scp:$xvector_dir/eva_1000/xvector.scp $xvector_dir/foo_40_4
  echo
  printf '% 12s' 'EER:'
  eer=$(awk '{print $3}' $xvector_dir/foo_40_4 | paste - $trl | awk '{print $1, $4}' | compute-eer - 2>/dev/null)
  printf '% 7.2f' $eer
  echo

  rm $xvector_dir/foo_*
fi

echo Done.