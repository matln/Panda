#!/bin/bash
# Copyright   2018  Tsinghua University (Author: Lantian Li)
# Apache 2.0.

# this recipe is used for end-to-end scoring.

nj=6
cmd="run.pl"
stage=-2
period=50

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# != 2 ]; then
  echo "Usage: $0 [opts] <nnet3-dir> <data-dir> "
  echo "  --nj <nj>   # number of parallel jobs"
  exit 1;
fi

dir=$1
data=$2

# check data.
for f in $data/feats.scp $data/vad.scp; do
  [ ! -f $f ] && echo "No such file $f" && exit 1;
done

name=`basename $data` 
sdata=$data/split$nj 
utils/split_data.sh $data $nj

if [ $stage -le -2 ]; then
  mkdir -p $dir/$name/log
  tmpdir=$dir/$name
  feats="ark:copy-feats scp:$sdata/JOB/feats.scp ark:- | select-voiced-frames ark:- scp:$sdata/JOB/vad.scp ark:- |"

  $cmd JOB=1:$nj $tmpdir/log/decode.JOB.log \
    nnet3-xvector-compute --xvector-period=$period --output-as-vector=true --use-gpu=no \
      $dir/final.raw "$feats" ark,scp:$tmpdir/xvector.JOB.ark,$tmpdir/xvector.JOB.scp
fi

for n in $(seq $nj); do
  cat $tmpdir/xvector.$n.scp || exit 1;
done > $tmpdir/xvector.scp

echo "Created decoding."