#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li
# this is the standard i-vector system.

. ./cmd.sh
. ./path.sh
set -e

stage=0
# number of components
cnum=2048
civ=400
clda=150
data=fisher_5000

# i-vector extraction on the evaluation set.
if [ $stage -le 0 ]; then
  lang=data/data_mfcc/chinese
  for sub in train-ch test-ch ; do
    sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=2G" --nj 10 \
      exp_ivector/$data/extractor_${cnum}_${civ} $lang/$sub \
      exp_ivector/$data/ivectors_${sub}_${cnum}_${civ}
  done

  lang=data/data_mfcc/uyghur
  for sub in train-uy test-uy ; do
    sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=2G" --nj 10 \
      exp_ivector/$data/extractor_${cnum}_${civ} $lang/$sub \
      exp_ivector/$data/ivectors_${sub}_${cnum}_${civ}
  done
fi

# i-vector evaluation

### long trials
if [ $stage -le 1 ]; then
  echo Long-Ch-Ch
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train-ch_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test-ch_${cnum}_${civ} \
    data/trials/long/ch-ch.trials

  echo Long-Uy-Uy
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train-uy_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test-uy_${cnum}_${civ} \
    data/trials/long/uy-uy.trials

  echo Long-Ch-Uy
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train-ch_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test-uy_${cnum}_${civ} \
    data/trials/long/ch-uy.trials

  echo Long-Uy-Ch
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train-uy_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test-ch_${cnum}_${civ} \
    data/trials/long/uy-ch.trials
fi

### short trials
if [ $stage -le 2 ]; then
  echo Short-Ch-Ch
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test-ch_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test-ch_${cnum}_${civ} \
    data/trials/short/ch-ch.trials

  echo Short-Uy-Uy
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test-uy_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test-uy_${cnum}_${civ} \
    data/trials/short/uy-uy.trials

  echo Short-Ch-Uy
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test-ch_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test-uy_${cnum}_${civ} \
    data/trials/short/ch-uy.trials
fi

echo Done.