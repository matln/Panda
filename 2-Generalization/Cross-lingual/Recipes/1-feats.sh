#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li

. ./cmd.sh
. ./path.sh
stage=0

set -e
mfccdir=`pwd`/_mfcc
vaddir=`pwd`/_vad
fbankdir=`pwd`/_fbank

if [ $stage -le 0 ]; then
  for data in chinese/train-ch chinese/test-ch uyghur/train-uy uyghur/test-uy; do
    # Make MFCCs.
    steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 20 --cmd "$train_cmd" \
      data/data_mfcc/$data exp_ivector/_log_mfcc $mfccdir
    # Make energy-vad.
    sid/compute_vad_decision.sh --nj 10 --cmd "$train_cmd" \
      data/data_mfcc/$data exp_ivector/_log_vad $vaddir

    # Make Fbanks.
    steps/make_fbank.sh --fbank-config conf/fbank.conf --nj 20 --cmd "$train_cmd" \
      data/data_fbank/$data exp_ctdnn/_log_fbank $fbankdir
    # Make CMVN.
    steps/compute_cmvn_stats.sh data/data_fbank/$data data/data_fbank/$data/_cmvn data/data_fbank/$data/local/cmvn

    cp data/data_mfcc/$data/vad.scp data/data_fbank/$data/vad.scp
  done
fi

echo Done.