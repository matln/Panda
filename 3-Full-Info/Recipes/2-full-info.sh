#!/bin/bash
# Copyright 2013   Daniel Povey
#           2018   Lantian Li
# this is the full-info training recipe, which is based on the standard d-vector system, built in nnet3.
# Once the basic CT-DNN model has been trained, then the full-info training can be started.

stage=0

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

data=fisher_5000
pnorm_input=2000
pnorm_output=400
init_dir=exp_ctdnn/$data/nnet3/cnn_4_8_pi${pnorm_input}_po${pnorm_output}_l6_splice10
dir=exp_ctdnn/$data/nnet3/cnn_4_8_pi${pnorm_input}_po${pnorm_output}_l6_splice10_mem_epochs${epochs}
train_data=data/data_fbank/$data

# for spk alignment and egs preparation, please refer to '1-基础模型/Recipes/3-dvector.sh'.
ali_dir=exp_ctdnn/$data/spk_ali
egs_dir=$init_dir/egs
iteration=6
epochs=2      # epochs in each iteration.


# data preparation on full-info training
if [ $stage -le 0 ]; then
  # generate utt2spk_num
  python local/spk_num.py $train_data
  # compute the spk-level d-vector based on the 0-th CT-DNN model as memory
  mkdir -p $dir/0/config
  cp $init_dir/final.raw $dir/0/
  cp $init_dir/config/vars $dir/0/config/
  sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir/0 $train_data $dir/0/$data
  copy-vector scp:$dir/0/$data/dvector.scp ark,t:$dir/0/$data/dvector.ark
fi

# full-info training
if [ $stage -le 1 ]; then
  for iter in `seq $iteration` ; do
    train_stage=-10
    echo START $iter.
    # E-steps: update CT-DNN model
    steps/nnet3/train_cnn_spk_vad_memory_epochs.sh --stage $train_stage \
      --num-epochs $epochs --num-jobs-initial 2 --num-jobs-final 8 \
      --splice-indexes "-4,-3,-2,-1,0,1,2,3,4  0  -2,2  0  -4,4  0" \
      --iters $iter --feat-type raw \
      --cmvn-opts "--norm-means=false --norm-vars=false" \
      --initial-effective-lrate 0.002 --final-effective-lrate 0.0002 \
      --cmd "$train_cmd" \
      --gpu-cmd "$gpu_cmd" \
      --pnorm-input-dim $pnorm_input \
      --pnorm-output-dim $pnorm_output \
      --egs-dir $egs_dir \
      --remove-egs false \
      $train_data $ali_dir $dir/$iter || exit 1;

    # M-steps: compute spk-level d-vector as memory
    sid/extract_dvectors.sh --nj 40 --cmd "$train_cmd" $dir/$iter $train_data $dir/$iter/$data
    copy-vector scp:$dir/$iter/$data/dvector.scp ark,t:$dir/$iter/$data/dvector.ark
  done
fi

# LDA/PLDA training
if [ $stage -le 2 ]; then
  for iter in `seq $iteration` ; do
    echo $iter
    # d-vector extraction on the training set.
    # sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir/$iter $train_data $dir/$iter/$data

    # compute LDA transform
    ivector-compute-lda --dim=$clda  --total-covariance-factor=0.1 \
      "ark:ivector-normalize-length scp:$dir/$iter/$data/dvector.scp ark:- |" \
      ark:$train_data/utt2spk \
      $dir/$iter/$data/transform_${clda}.mat

    # compute PLDA transform
    ivector-compute-plda ark:$train_data/spk2utt \
      "ark:ivector-normalize-length scp:$dir/$iter/$data/dvector.scp ark:- |" \
      $dir/$iter/$data/plda 2>$dir/$iter/$data/log/plda.log
  done
fi

# d-vector extraction on the evaluation set.
if [ $stage -le 3 ]; then
  path=data/data_fbank/fisher_test
  for iter in `seq $iteration` ; do
    echo $iter
    for eva in train_all test_3 test_9 test_18 test_30; do
      sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir/$iter $path/$eva $dir/$iter/$eva
    done

    for eva in test_short_20 test_short_50 test_short_100; do
      sid/extract_dvectors_seg.sh --nj 20 --cmd "$train_cmd" $dir/$iter $path/$eva $dir/$iter/$eva
    done
  done
fi


# d-vector evaluation
if [ $stage -le 4 ]; then
  for iter in `seq $iteration` ; do
    echo $iter
    echo 30-20f
    local/evaluate_dv.sh $dir/$iter/$data \
      $dir/$iter/train_all \
      $dir/$iter/test_short_20 \
      data/trials/30-20f.trl

    echo 30-50f
    local/evaluate_dv.sh $dir/$iter/$data \
      $dir/$iter/train_all \
      $dir/$iter/test_short_50 \
      data/trials/30-50f.trl

    echo 30-100f
    local/evaluate_dv.sh $dir/$iter/$data \
      $dir/$iter/train_all \
      $dir/$iter/test_short_100 \
      data/trials/30-100f.trl

    echo 30-3
    local/evaluate_dv.sh $dir/$iter/$data \
      $dir/$iter/train_all \
      $dir/$iter/test_3 \
      data/trials/30-3.trl

    echo 30-9
    local/evaluate_dv.sh $dir/$iter/$data \
      $dir/$iter/train_all \
      $dir/$iter/test_9 \
      data/trials/30-9.trl

    echo 30-18
    local/evaluate_dv.sh $dir/$iter/$data \
      $dir/$iter/train_all \
      $dir/$iter/test_18 \
      data/trials/30-18.trl

    echo 30-30
    local/evaluate_dv.sh $dir/$iter/$data \
      $dir/$iter/train_all \
      $dir/$iter/test_30 \
      data/trials/30-30.trl
fi

echo Done.
