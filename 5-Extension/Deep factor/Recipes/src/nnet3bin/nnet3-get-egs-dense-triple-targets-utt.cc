// nnet3bin/nnet3-get-egs-dense-triple-targets-utt.cc

// Copyright 2012-2015  Johns Hopkins University (author:  Daniel Povey)
//           2014-2015  Vimal Manohar
//                2018  Lantian Li
// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#include <sstream>

#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "hmm/transition-model.h"
#include "hmm/posterior.h"
#include "nnet3/nnet-example.h"

namespace kaldi {
namespace nnet3 {


static void ProcessFile(const MatrixBase<BaseFloat> &feats_1,
                        const MatrixBase<BaseFloat> &feats_2,
                        const MatrixBase<BaseFloat> &feats_3,
   			const MatrixBase<BaseFloat> *ivector_feats,
                        const MatrixBase<BaseFloat> &targets,
                        const std::string &utt_id,
                        bool compress,
                        int32 num_targets,
                        int32 left_context_1,
                        int32 right_context_1,
                        int32 left_context_2,
                        int32 right_context_2,
                        int32 left_context_3,
                        int32 right_context_3,
                        int32 frames_per_eg,
                        int64 *num_frames_written,
                        int64 *num_egs_written,
                        NnetExampleWriter *example_writer) {
  KALDI_ASSERT(feats_1.NumRows() == feats_2.NumRows());
  KALDI_ASSERT(feats_1.NumRows() == feats_3.NumRows());
  KALDI_ASSERT(feats_1.NumRows() == static_cast<int32>(targets.NumRows()));
  
  for (int32 t = 0; t < feats_1.NumRows(); t += frames_per_eg) {

    // actual_frames_per_eg is the number of frames with actual targets.
    // At the end of the file, we pad with the last frame repeated
    // so that all examples have the same structure (prevents the need
    // for recompilations).
    // TODO: We might need to ignore the end of the file.
    int32 actual_frames_per_eg = std::min(frames_per_eg,
                                          feats_1.NumRows() - t);


    int32 tot_frames_1 = left_context_1 + frames_per_eg + right_context_1;
    int32 tot_frames_2 = left_context_2 + frames_per_eg + right_context_2;
    int32 tot_frames_3 = left_context_3 + frames_per_eg + right_context_3;

    Matrix<BaseFloat> input_frames_1(tot_frames_1, feats_1.NumCols());
    Matrix<BaseFloat> input_frames_2(tot_frames_2, feats_2.NumCols());
    Matrix<BaseFloat> input_frames_3(tot_frames_3, feats_3.NumCols());
    
    // Set up "input_frames".
    for (int32 j = -left_context_1; j < frames_per_eg + right_context_1; j++) {
      int32 t2 = j + t;
      if (t2 < 0) t2 = 0;
      if (t2 >= feats_1.NumRows()) t2 = feats_1.NumRows() - 1;
      SubVector<BaseFloat> src(feats_1, t2),
          dest(input_frames_1, j + left_context_1);
      dest.CopyFromVec(src);
    }

    for (int32 j = -left_context_2; j < frames_per_eg + right_context_2; j++) {
      int32 t2 = j + t;
      if (t2 < 0) t2 = 0;
      if (t2 >= feats_2.NumRows()) t2 = feats_2.NumRows() - 1;
      SubVector<BaseFloat> src(feats_2, t2),
          dest(input_frames_2, j + left_context_2);
      dest.CopyFromVec(src);
    }

    for (int32 j = -left_context_3; j < frames_per_eg + right_context_3; j++) {
      int32 t2 = j + t;
      if (t2 < 0) t2 = 0;
      if (t2 >= feats_3.NumRows()) t2 = feats_3.NumRows() - 1;
      SubVector<BaseFloat> src(feats_3, t2),
          dest(input_frames_3, j + left_context_3);
      dest.CopyFromVec(src);
    } 

    NnetExample eg;
    
    // call the regular input "input".
    eg.io.push_back(NnetIo("input", - left_context_1,
                           input_frames_1));
    eg.io.push_back(NnetIo("input_A", - left_context_2,
                           input_frames_2));
    eg.io.push_back(NnetIo("input_B", - left_context_3,
                           input_frames_3));

    // if applicable, add the iVector feature.
    if (ivector_feats != NULL) {
      // try to get closest frame to middle of window to get
      // a representative iVector.
      int32 closest_frame = t + (actual_frames_per_eg / 2);
      KALDI_ASSERT(ivector_feats->NumRows() > 0);
      if (closest_frame >= ivector_feats->NumRows())
        closest_frame = ivector_feats->NumRows() - 1;
      Matrix<BaseFloat> ivector(1, ivector_feats->NumCols());
      ivector.Row(0).CopyFromVec(ivector_feats->Row(closest_frame));
      eg.io.push_back(NnetIo("ivector", 0, ivector));
    }

    // add the labels.
    Matrix<BaseFloat> targets_dest(frames_per_eg, targets.NumCols());
    for (int32 i = 0; i < actual_frames_per_eg; i++) {
      // Copy the i^th row of the target matrix from the (t+i)^th row of the
      // input targets matrix
      SubVector<BaseFloat> this_target_dest(targets_dest, i);
      SubVector<BaseFloat> this_target_src(targets, t+i);
      this_target_dest.CopyFromVec(this_target_src);
    } 
    
    // Copy the last frame's target to the padded frames
    for (int32 i = actual_frames_per_eg; i < frames_per_eg; i++) {
      // Copy the i^th row of the target matrix from the last row of the 
      // input targets matrix
      KALDI_ASSERT(t + actual_frames_per_eg - 1 == feats_1.NumRows() - 1);
      SubVector<BaseFloat> this_target_dest(targets_dest, i);
      SubVector<BaseFloat> this_target_src(targets, t+actual_frames_per_eg-1);
      this_target_dest.CopyFromVec(this_target_src);
    } 

    // push this created targets matrix into the eg
    eg.io.push_back(NnetIo("output", 0, targets_dest));
    
    if (compress)
      eg.Compress();
      
    std::ostringstream os;
    os << utt_id ;

    std::string key = os.str(); // key is <utt_id>-<frame_id>

    *num_frames_written += actual_frames_per_eg;
    *num_egs_written += 1;

    example_writer->Write(key, eg);
  }
}


} // namespace nnet2
} // namespace kaldi

int main(int argc, char *argv[]) {
  try {
    using namespace kaldi;
    using namespace kaldi::nnet3;
    typedef kaldi::int32 int32;
    typedef kaldi::int64 int64;

    const char *usage =
        "Get frame-by-frame examples of data for nnet3 neural network training.\n"
        "This program is similar to nnet3-get-egs, but the targets here are "
        "dense matrices instead of posteriors (sparse matrices).\n"
        "This is useful when you want the targets to be continuous real-valued "
        "with the neural network possibly trained with a quadratic objective\n"
        "\n"
        "Usage:  nnet3-get-egs-dense-triple-targets --num-targets=<n> [options] "
        "<features-rspecifier-1> <features-rspecifier-2> <features-rspecifier-3> <targets-rspecifier> <egs-out>\n"
        "\n"
        "An example [where $feats expands to the actual features]:\n"
        "nnet3-get-egs-dense-triple-targets --num-targets=26 --left-context-1=12 --right-context-1=9 --left-context-2=12 --right-context-2=9\\\n"
        "--left-context-3=12 --right-context-3=9 --num-frames=8 \"$feats_1\" \"$feats_2\" \"$feats_3\" \\\n"
        "\"ark:copy-matrix ark:exp/snrs/snr.1.ark ark:- |\"\n"
        "   ark:- \n";
        

    bool compress = true;
    int32 num_targets = -1, left_context_1 = 0, right_context_1 = 0, left_context_2 = 0, right_context_2 = 0, left_context_3 = 0, right_context_3 = 0,
        num_frames = 1, length_tolerance = 100;
        
    std::string ivector_rspecifier;
    
    ParseOptions po(usage);
    po.Register("compress", &compress, "If true, write egs in "
                "compressed format.");
    po.Register("num-targets", &num_targets, "Number of targets for the neural network");
    po.Register("left-context-1", &left_context_1, "Number of frames of left "
                "context the neural net requires.");
    po.Register("right-context-1", &right_context_1, "Number of frames of right "
                "context the neural net requires.");
    po.Register("left-context-2", &left_context_2, "Number of frames of left "
                "context the neural net requires.");
    po.Register("right-context-2", &right_context_2, "Number of frames of right "
                "context the neural net requires.");
    po.Register("left-context-3", &left_context_3, "Number of frames of left "
                "context the neural net requires.");
    po.Register("right-context-3", &right_context_3, "Number of frames of right "
                "context the neural net requires.");
    po.Register("num-frames", &num_frames, "Number of frames with labels "
                "that each example contains.");
    po.Register("ivectors", &ivector_rspecifier, "Rspecifier of ivector "
                "features, as matrix.");
    po.Register("length-tolerance", &length_tolerance, "Tolerance for "
                "difference in num-frames between feat and ivector matrices");
    
    po.Read(argc, argv);

    if (po.NumArgs() != 5) {
      po.PrintUsage();
      exit(1);
    }

    if (num_targets <= 0)
      KALDI_ERR << "--num-targets options is required.";

    std::string feature_rspecifier_1 = po.GetArg(1),
        feature_rspecifier_2 = po.GetArg(2),
        feature_rspecifier_3 = po.GetArg(3),
        matrix_rspecifier = po.GetArg(4),
        examples_wspecifier = po.GetArg(5);

    // Read in all the training files.
    SequentialBaseFloatMatrixReader feat_reader_1(feature_rspecifier_1);
    RandomAccessBaseFloatMatrixReader feat_reader_2(feature_rspecifier_2);
    RandomAccessBaseFloatMatrixReader feat_reader_3(feature_rspecifier_3);
    RandomAccessBaseFloatMatrixReader matrix_reader(matrix_rspecifier);
    NnetExampleWriter example_writer(examples_wspecifier);
    RandomAccessBaseFloatMatrixReader ivector_reader(ivector_rspecifier);
    
    int32 num_done = 0, num_err = 0;
    int64 num_frames_written = 0, num_egs_written = 0;
    
    for (; !feat_reader_1.Done(); feat_reader_1.Next()) {
      std::string key = feat_reader_1.Key();
      const Matrix<BaseFloat> &feats_1 = feat_reader_1.Value();
      if (!feat_reader_2.HasKey(key)) {
        KALDI_WARN << "Second table has no feature for key "
                   << key;
        num_err++;
        continue;
      }

      if (!feat_reader_3.HasKey(key)) {
        KALDI_WARN << "Thrid table has no feature for key "
                   << key;
        num_err++;
        continue;
      }

      Matrix<BaseFloat> feats_2 (feat_reader_2.Value(key));
      if (feats_1.NumRows() != feats_2.NumRows()) {
        KALDI_WARN << "Mismatch in number for frames " << feats_1.NumRows()
                   << " for features-1 and features-2 " << feats_2.NumRows()
                   << " for key " << key;
        num_err++;
        continue;
      }

      Matrix<BaseFloat> feats_3 (feat_reader_3.Value(key));
      if (feats_1.NumRows() != feats_3.NumRows()) {
        KALDI_WARN << "Mismatch in number for frames " << feats_1.NumRows()
                   << " for features-1 and features-3 " << feats_3.NumRows()
                   << " for key " << key;
        num_err++;
        continue;
      }

      if (!matrix_reader.HasKey(key)) {
        KALDI_WARN << "No target matrix for key " << key;
        num_err++;
      } else {
        const Matrix<BaseFloat> &target_matrix = matrix_reader.Value(key);
        if (target_matrix.NumRows() != feats_1.NumRows()) {
          KALDI_WARN << "Target matrix has wrong size " 
                     << target_matrix.NumRows()
                     << " versus " << feats_1.NumRows();
          num_err++;
          continue;
        }
        const Matrix<BaseFloat> *ivector_feats = NULL;
        if (!ivector_rspecifier.empty()) {
          if (!ivector_reader.HasKey(key)) {
            KALDI_WARN << "No iVectors for utterance " << key;
            num_err++;
            continue;
          } else {
            // this address will be valid until we call HasKey() or Value()
            // again.
            ivector_feats = &(ivector_reader.Value(key));
          }
        }

        if (ivector_feats != NULL &&
            (abs(feats_1.NumRows() - ivector_feats->NumRows()) > length_tolerance
             || ivector_feats->NumRows() == 0)) {
          KALDI_WARN << "Length difference between feats " << feats_1.NumRows()
                     << " and iVectors " << ivector_feats->NumRows()
                     << "exceeds tolerance " << length_tolerance;
          num_err++;
          continue;
        }
	      num_frames = feats_1.NumRows();          
        ProcessFile(feats_1, feats_2, feats_3, ivector_feats, target_matrix, key, compress,
                    num_targets, left_context_1, right_context_1, left_context_2, right_context_2, left_context_3, right_context_3, num_frames,
                    &num_frames_written, &num_egs_written,
                    &example_writer);
        num_done++;
      }
    }

    KALDI_LOG << "Finished generating examples, "
              << "successfully processed " << num_done
              << " feature files, wrote " << num_egs_written << " examples, "
              << " with " << num_frames_written << " egs in total; "
              << num_err << " files had errors.";
    return (num_egs_written == 0 || num_err > num_done ? 1 : 0);
  } catch(const std::exception &e) {
    std::cerr << e.what() << '\n';
    return -1;
  }
}

