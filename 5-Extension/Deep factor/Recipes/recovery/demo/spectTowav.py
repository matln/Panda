#!/usr/bin/python

# Copyright  2018  Lantian Li
# Apache 2.0.

# this script is used to convert spectrum to waveform.

import numpy as np
import librosa
import wave
import sys

def time_signal_2_logmag_phase(time_signal, n_fft, hop_length):
    """ 时域信号 --> 对数幅度谱 + 相位谱
        Args:
            n_fft: 帧长
            hop_length: 帧移
        Return:
            logmag, phase
            logmag 为 对数幅度谱
            phase 为 相位谱
    """
    spec = librosa.stft(time_signal, n_fft = n_fft, hop_length = hop_length)
    tmp = np.log(spec)
    logmag = np.real(tmp)
    phase = np.imag(tmp)
    return logmag, phase, 2*logmag

def logmag_phase_2_time_signal(logmag, phase, hop_length):
    """ 对数幅度谱 + 相位谱 --> 时域信号
        Args：
            logmag: 对数幅度谱
            phase: 相位谱
            hop_length: 帧移
        Return:
            恢复的时域信号
    """
    return librosa.istft(np.exp(logmag/2 + phase * 1j), hop_length = hop_length)

def demo(wavin, specin, wavout):
    f1 = wave.open(wavin, 'r')
    f2 = pp = f1.readframes(f1.getparams()[3])
    f3 = np.fromstring(f2, dtype = np.short)
    logmag, phase, kk = time_signal_2_logmag_phase(f3, n_fft = 256, hop_length = 80)

    fin = specin
    fin = open(fin, 'r')
    lines = fin.readlines()
    fin.close()
    _list = []
    for line in lines:
        part = line.split() 
        temp = [float(i) for i in part]
        _list.append(temp)
    specd = np.transpose(_list)
    framelen = len(specd[0])

    # Note: 	
    # for raw and recovery (specd - 20.7944)
    # recov_wav = logmag_phase_2_time_signal(specd - 20.7944, phase[:,:framelen], hop_length=80)
    
    # for phone, speaker and emotion (specd - 7)
    recov_wav = logmag_phase_2_time_signal(specd - 7, phase[:,:framelen], hop_length=80)
    librosa.output.write_wav(wavout, recov_wav, sr = 8000)

if __name__ == '__main__':
    factor = 'data/phone'
    wavin = 'data/CHEAVD_1_1_E02_001_worried.wav'
    specin = factor + '.ark'
    wavout = factor + '.wav'
    demo(wavin, specin, wavout)
