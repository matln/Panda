#!/usr/bin/python

# Copyright  2018  Lantian Li
# Apache 2.0.

# this script is used to convert vad.ark to ali.ark

import sys

if __name__ =="__main__":
	fin_vad = sys.argv[1]
	fin_type = sys.argv[2]
	fout_ali = sys.argv[3]
	fin = open(fin_vad, 'r')
	lines = fin.readlines()
    fin.close()
	fout = open(fout_ali, 'w')
   	for line in lines:
       	parts = line.split()
		fout.write(parts[0] + ' ')
		if fin_type == '1':
			for i in range(1, len(parts)):
				if parts[i] == '1':
					fout.write(parts[i] + ' ')
		elif fin_type == '0':
			for i in range(1, len(parts)):
				if parts[i] == '0':
					fout.write(parts[i] + ' ')
		else:
			for i in range(1, len(parts)):
				if parts[i] == '1' or parts[i] == '0':
					fout.write(parts[i] + ' ')
		fout.write('\n')
	fout.close()