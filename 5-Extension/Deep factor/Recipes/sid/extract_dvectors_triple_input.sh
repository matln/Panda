#!/bin/bash
# Copyright   2018  Tsinghua University (Author: Lantian Li)
# Apache 2.0.

# this recipe is used for PAT d-vector extraction.

nj=6
cmd="run.pl"
stage=-2

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# != 5 ]; then
  echo "Usage: $0 [opts] <nnet3-dir> <data-dir> <phone-dir> <spk-dir> <dve-dir>"
  echo "  --nj <nj>   # number of parallel jobs"
  echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs."
  exit 1;
fi

dir=$1
data=$2
pdata=$3
sdata=$4
dvec=$5

# check data.
for f in $data/feats.scp $data/vad.scp $pdata/feats.scp $sdata/feats.scp $dir/configs/vars ; do
  [ ! -f $f ] && echo "No such file $f" && exit 1;
done

sdata=$data/split$nj 
utils/split_data.sh $data $nj

pdata=$phone/split$nj
utils/split_data.sh $phone $nj

hdata=$spk/split$nj
utils/split_data.sh $spk $nj

# generate alignment.
if [ $stage -le -2 ]; then
  if [ ! -f $data/ali.scp ]; then
    copy-vector scp:$data/vad.scp ark,t:$data/vad.ark
    python local/genAli.py $data/vad.ark 1 $data/tmp.ark
    copy-int-vector ark:$data/tmp.ark ark,scp:$data/ali.ark,$data/ali.scp
    rm $data/vad.ark $data/tmp.ark
  fi
fi

# utt-level decode.
if [ $stage -le -1 ]; then
  mkdir -p $dvec/log
  cmvn_opts=$(cat $dir/cmvn_opts)
  echo $cmvn_opts
  cmvn_opts_p=$(cat $dir/cmvn_opts_p)
  echo $cmvn_opts_p
  cmvn_opts_s=$(cat $dir/cmvn_opts_s)
  echo $cmvn_opts_s
  
  feats="ark:copy-feats scp:$sdata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp ark:- ark:- | select-voiced-frames ark:- scp:$sdata/JOB/vad.scp ark:- |"
  feats_phone="ark:copy-feats scp:$pdata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts_p --utt2spk=ark:$pdata/JOB/utt2spk scp:$pdata/JOB/cmvn.scp ark:- ark:- | select-voiced-frames ark:- scp:$sdata/JOB/vad.scp ark:- |"
  feats_spk="ark:copy-feats scp:$hdata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts_s --utt2spk=ark:$hdata/JOB/utt2spk scp:$hdata/JOB/cmvn.scp ark:- ark:- | select-voiced-frames ark:- scp:$sdata/JOB/vad.scp ark:- |"
  post="ark,s,cs:utils/filter_scp.pl $sdata/JOB/utt2spk $data/ali.scp | ali-to-post scp:- ark:- |"

  . $dir/configs/vars
  echo $nj
  $cmd JOB=1:$nj $dvec/log/decode.JOB.log \
    nnet3-get-egs-triple-input-utt --compress=false --num-pdfs=$num_targets \
      --left-context-1=$model_left_context_1 --right-context-1=$model_right_context_1 \
      --left-context-2=$model_left_context_2 --right-context-2=$model_right_context_2 \
      --left-context-3=$model_left_context_3 --right-context-3=$model_right_context_3 \
      "$feats" "$feats_phone" "$feats_spk" "$post" ark:- \| \
    nnet3-compute-from-egs --apply-exp=true --use-gpu=no $dir/final.raw ark:- ark:- \| \
    matrix-sum-rows ark:- ark,t:$dvec/dvector.JOB.ark
  wait;

  for n in $(seq $nj); do
    cat $dvec/dvector.$n.ark || exit 1;
  done > $dvec/dvector.ark
fi

echo "Created decoding "

