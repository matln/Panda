#!/bin/bash
# Copyright   2018  Tsinghua University (Author: Lantian Li)
# Apache 2.0.

# this recipe is used for PAT d-vector extraction.

nj=6
cmd="run.pl"
stage=-2

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# != 4 ]; then
  echo "Usage: $0 [opts] <nnet3-dir> <data-dir> <phone-dir> <dve-dir>"
  echo "  --nj <nj>   # number of parallel jobs"
  echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs."
  exit 1;
fi

dir=$1
data=$2
pdata=$3
dvec=$4

# check data.
for f in $data/feats.scp $data/vad.scp $pdata/feats.scp $pdata/vad.scp $dir/configs/vars ; do
  [ ! -f $f ] && echo "No such file $f" && exit 1;
done

sdata=$data/split$nj 
utils/split_data.sh $data $nj

spdata=$pdata/split$nj 
utils/split_data.sh $pdata $nj

# generate alignment.
if [ $stage -le -2 ]; then
  if [ ! -f $data/ali.scp ]; then
    copy-vector scp:$data/vad.scp ark,t:$data/vad.ark
    python local/genAli.py $data/vad.ark 1 $data/tmp.ark
    copy-int-vector ark:$data/tmp.ark ark,scp:$data/ali.ark,$data/ali.scp
    rm $data/vad.ark $data/tmp.ark
  fi
fi

# utt-level decode.
if [ $stage -le -1 ]; then
  mkdir -p $dvec/log
  cmvn_opts=$(cat $dir/cmvn_opts)
  echo $cmvn_opts
  cmvn_opts_p=$(cat $dir/cmvn_opts_p)
  echo $cmvn_opts_p
  feats="ark:copy-feats scp:$sdata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp ark:- ark:- | select-voiced-frames ark:- scp:$sdata/JOB/vad.scp ark:- |"
  feats_phone="ark:copy-feats scp:$spdata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts_p --utt2spk=ark:$spdata/JOB/utt2spk scp:$spdata/JOB/cmvn.scp ark:- ark:- | select-voiced-frames ark:- scp:$spdata/JOB/vad.scp ark:- |"
  post="ark,s,cs:utils/filter_scp.pl $sdata/JOB/utt2spk $data/ali.scp | ali-to-post scp:- ark:- |"

  . $dir/configs/vars
  echo $nj
  $cmd JOB=1:$nj $dvec/log/decode.JOB.log \
    nnet3-get-egs-double-input-utt --compress=false --num-pdfs=$num_targets \
      --left-context-1=$model_left_context_1 --right-context-1=$model_right_context_1 \
      --left-context-2=$model_left_context_2 --right-context-2=$model_right_context_2 \
      "$feats" "$feats_phone" "$post" ark:- \| \
    nnet3-compute-from-egs --apply-exp=true --use-gpu=no $dir/final.raw ark:- ark:- \| \
    matrix-sum-rows ark:- ark,t:$dvec/dvector.JOB.ark
  wait;
  
  for n in $(seq $nj); do
    cat $dvec/dvector.$n.ark || exit 1;
  done > $dvec/dvector.ark
fi
echo "Created decoding."
