#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li

. ./cmd.sh
. ./path.sh
stage=0

set -e
mfccdir=`pwd`/_mfcc
vaddir=`pwd`/_vad
fbankdir=`pwd`/_fbank
spectdir=`pwd`/_spect

if [ $stage -le 0 ]; then
  for data in train test ; do
    # Make MFCCs.
    steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 20 --cmd "$train_cmd" \
      data/data_mfcc/$data _log_mfcc $mfccdir
    # Make energy-vad.
    sid/compute_vad_decision.sh --nj 10 --cmd "$train_cmd" \
      data/data_mfcc/$data _log_vad $vaddir

    # Make Fbanks.
    steps/make_fbank.sh --fbank-config conf/fbank.conf --nj 20 --cmd "$train_cmd" \
      data/data_fbank/$data _log_fbank $fbankdir
    # Make CMVN.
    steps/compute_cmvn_stats.sh data/data_fbank/$data data/data_fbank/$data/_cmvn data/data_fbank/$data/local/cmvn
    cp data/data_mfcc/$data/vad.scp data/data_fbank/$data/vad.scp

    # Make spectrum.
    steps/make_spectrum.sh --spect-config conf/spect.conf --nj 20 --cmd "$train_cmd" \
      data/data_spect/$data _log_spect $spectdir
    steps/compute_cmvn_stats.sh data/data_spect/$data data/data_spect/$data/_cmvn \
      data/data_spect/$data/local/cmvn
  done
fi

# prepare alignment
if [ $stage -le 1 ]; then
  train_data=data/data_fbank/train
  # generate feats.len
  feat-to-len scp:$train_data/feats.scp ark,t:$train_data/feats.len
  # generate vad.ark
  copy-vector scp:$train_data/vad.scp ark,t:$train_data/vad.ark
  # generate ali.ark, spk_counts and spk_num
  mkdir -p exp/spk_ali
  python local/spk_ali.py $train_data exp/spk_ali
fi

# phonetic feature extraction.
if [ $stage -le 2 ]; then
  phone_dir=phone/wsj
  for data in train test ; do
    steps/make_phone_feats.sh --cmd "$train_cmd" --nj 10 --no-softmax true \
      data/data_fbank/$data $phone_dir $phone_dir/$data
    cp data/data_fbank/$data/vad.scp $phone_dir/$data/
    cp data/data_fbank/$data/spk2utt $phone_dir/$data/
    cp data/data_fbank/$data/utt2spk $phone_dir/$data/
    steps/compute_cmvn_stats.sh $phone_dir/$data $phone_dir/$data/_cmvn $phone_dir/$data/local/cmvn
  done
fi

# speaker feature extraction.
if [ $stage -le 3 ]; then
  spk_dir=speaker/fisher_5000
  for data in train test ; do
    steps/make_spk_feats.sh --cmd "$train_cmd" --nj 10 $spk_dir \
      data/data_fbank/$data phone/wsj/$data $spk_dir/$data
    cp data/data_fbank/$data/vad.scp $spk_dir/$data/
    cp data/data_fbank/$data/spk2utt $spk_dir/$data/
    cp data/data_fbank/$data/utt2spk $spk_dir/$data/
    steps/compute_cmvn_stats.sh $spk_dir/$data $spk_dir/$data/_cmvn $spk_dir/$data/local/cmvn
  done
fi

echo Done.