#!/bin/bash
# Copyright 2012-2013 Karel Vesely, Daniel Povey
#           2018      Lantian Li
# Apache 2.0
# this script is used to compute the phone-level posteriors.

# Begin configuration section. 
nnet=               # non-default location of DNN (optional)
feature_transform=  # non-default location of feature_transform (optional)
model=              # non-default location of transition model (optional)
srcdir=             # non-default location of DNN-dir (decouples model dir from decode dir)

stage=0 # stage=1 skips lattice generation
nj=10
cmd=run.pl
use_gpu="no" # yes|no|optionaly
no_softmax=true

echo "$0 $@"  # Print the command line for logging

[ -f ./path.sh ] && . ./path.sh; # source the path.
. parse_options.sh || exit 1;

if [ $# != 3 ]; then
   echo "Usage: $0 [options] <data-dir> <decode-dir> <save-dir>"
   echo "... where <decode-dir> is assumed to be a sub-directory of the directory"
   echo " where the DNN and transition model is."
   echo "e.g.: $0 exp/dnn1/graph_tgpr data/test exp/dnn1/decode_tgpr"
   echo ""
   echo "This script works on plain or modified features (CMN,delta+delta-delta),"
   echo "which are then sent through feature-transform. It works out what type"
   echo "of features you used from content of srcdir."
   echo ""
   echo "main options (for others, see top of script file)"
   echo "  --config <config-file>                           # config containing options"
   echo "  --nj <nj>                                        # number of parallel jobs"
   echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs."
   echo ""
   echo "  --nnet <nnet>                                    # non-default location of DNN (opt.)"
   echo "  --srcdir <dir>                                   # non-default dir with DNN/models, can be different"
   echo "                                                   # from parent dir of <decode-dir>' (opt.)"
   echo ""
   echo "  --num-threads <N>                                # N>1: run multi-threaded decoder"
   exit 1;
fi

data=$1
nnetdir=$2
dir=$3

phones=`cat $nnetdir/sets.int`

[ -z $srcdir ] && srcdir=$nnetdir; # Default model directory one level up from decoding directory.
sdata=$data/split$nj;

mkdir -p $dir/log
mkdir -p $dir/_phone

[[ -d $sdata && $data/feats.scp -ot $sdata ]] || split_data.sh $data $nj || exit 1;
echo $nj > $dir/num_jobs

# Select default locations to model files (if not already set externally)
if [ -z "$nnet" ]; then nnet=$srcdir/final.nnet; fi
if [ -z "$model" ]; then model=$srcdir/final.mdl; fi
if [ -z "$feature_transform" ]; then feature_transform=$srcdir/final.feature_transform; fi

# Check that files exist
for f in $data/feats.scp $nnet $model $feature_transform; do
  [ ! -f $f ] && echo "$0: missing file $f" && exit 1;
done

# PREPARE FEATURE EXTRACTION PIPELINE
# Create the feature stream:
feats="ark,s,cs:copy-feats scp:$sdata/JOB/feats.scp ark:- |"

# Optionally add cmvn
feats="$feats apply-cmvn --print-args=false --norm-vars=false --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp ark:- ark:- |"

# Run the decoding in the queue
if [ $stage -le 0 ]; then
  $cmd JOB=1:$nj $dir/log/decode.JOB.log \
    nnet-forward-phone --feature-transform=$feature_transform --use-gpu=$use_gpu --no-softmax=$no_softmax $nnet $model "$phones" "$feats" ark,scp:$dir/_phone/feats.JOB.ark,$dir/_phone/feats.JOB.scp  || exit 1;
fi

# concatenate the .scp files together.
for n in $(seq $nj); do
  cat $dir/_phone/feats.$n.scp || exit 1;
done > $dir/feats.scp

exit 0;
