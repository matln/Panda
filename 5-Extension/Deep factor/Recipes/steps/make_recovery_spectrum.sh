#!/bin/bash
# Copyright   2018  Tsinghua University (Author: Lantian Li)
# Apache 2.0.

# this recipe is used for recovery spectrum extraction.

nj=6
cmd="run.pl"
stage=-2

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# != 5 ]; then
  echo "Usage: $0 [opts] <nnet3-dir> <phone-dir> <spk-dir> <emo-dir> <ali-dir>"
  echo "  --nj <nj>   # number of parallel jobs"
  echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs."
  exit 1;
fi

dir=$1
phone=$2
spk=$3
emotion=$4
alidir=$5
name=`basename $phone`

# check data.
for f in $phone/feats.scp $spk/feats.scp $emotion/feats.scp $alidir/feats.scp $dir/configs/vars ; do
  [ ! -f $f ] && echo "No such file $f" && exit 1;
done

pdata=$phone/split$nj
utils/split_data.sh $phone $nj
sdata=$spk/split$nj
utils/split_data.sh $spk $nj
edata=$emotion/split$nj
utils/split_data.sh $emotion $nj
tdata=$alidir/split$nj
utils/split_data.sh $alidir $nj

cmvn_opts_p=$(cat $dir/cmvn_opts_p)
echo $cmvn_opts_p
cmvn_opts_s=$(cat $dir/cmvn_opts_s)
echo $cmvn_opts_s
cmvn_opts_e=$(cat $dir/cmvn_opts_e)
echo $cmvn_opts_e
cmvn_opts_t=$(cat $dir/cmvn_opts_t)
echo $cmvn_opts_t

. $dir/configs/vars

echo --left-context-1=$model_left_context_1
echo --right-context-1=$model_right_context_1
echo --left-context-2=$model_left_context_2
echo --right-context-2=$model_right_context_2
echo --left-context-3=$model_left_context_3
echo --right-context-3=$model_right_context_3

feats_phone="ark:copy-feats scp:$pdata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts_p --utt2spk=ark:$pdata/JOB/utt2spk scp:$pdata/JOB/cmvn.scp ark:- ark:- |"
feats_spk="ark:copy-feats scp:$sdata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts_s --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp ark:- ark:- |"
feats_emo="ark:copy-feats scp:$edata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts_e --utt2spk=ark:$edata/JOB/utt2spk scp:$edata/JOB/cmvn.scp ark:- ark:- |"
feats_targets="ark:copy-feats scp:$tdata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts_t --utt2spk=ark:$tdata/JOB/utt2spk scp:$tdata/JOB/cmvn.scp ark:- ark:- |"

echo $nj

if [ $stage -le -2 ]; then
  echo decoding recovery spectrum.
  mkdir -p $dir/${name}_recovery/log
  tmpdir=$dir/${name}_recovery
  $cmd JOB=1:$nj $tmpdir/log/decode.JOB.log \
    nnet3-get-egs-dense-triple-targets-utt --compress=false --num-targets=129 \
      --left-context-1=$model_left_context_1 --right-context-1=$model_right_context_1 \
      --left-context-2=$model_left_context_2 --right-context-2=$model_right_context_2 \
      --left-context-3=$model_left_context_3 --right-context-3=$model_right_context_3 \
      "$feats_phone" "$feats_spk" "$feats_emo" "$feats_targets" ark:- \| \
    nnet3-compute-from-egs --use-gpu=no $dir/final.raw ark:- ark,scp:$tmpdir/feats.JOB.ark,$tmpdir/feats.JOB.scp
  wait;
  for n in $(seq $nj); do
    cat $tmpdir/feats.$n.scp || exit 1;
  done > $tmpdir/feats.scp
fi

if [ $stage -le -1 ]; then
  echo decoding phone spectrum.
  nnet3-copy --binary=false $dir/final.raw $dir/final.hid_phone
  sed -i "s/input=combine/input=final-affine-p/g" $dir/final.hid_phone
  mkdir -p $dir/${name}_phone/log
  tmpdir=$dir/${name}_phone
  $cmd JOB=1:$nj $tmpdir/log/decode.JOB.log \
    nnet3-get-egs-dense-triple-targets-utt --compress=false --num-targets=129 \
      --left-context-1=$model_left_context_1 --right-context-1=$model_right_context_1 \
      --left-context-2=$model_left_context_2 --right-context-2=$model_right_context_2 \
      --left-context-3=$model_left_context_3 --right-context-3=$model_right_context_3 \
      "$feats_phone" "$feats_spk" "$feats_emo" "$feats_targets" ark:- \| \
    nnet3-compute-from-egs --use-gpu=no $dir/final.hid_phone ark:- ark,scp:$tmpdir/feats.JOB.ark,$tmpdir/feats.JOB.scp
  wait;
  for n in $(seq $nj); do
    cat $tmpdir/feats.$n.scp || exit 1;
  done > $tmpdir/feats.scp
  rm $dir/final.hid_phone
fi

if [ $stage -le 0 ]; then
  echo decoding speaker spectrum.
  nnet3-copy --binary=false $dir/final.raw $dir/final.hid_spk
  sed -i "s/input=combine/input=final-affine-s/g" $dir/final.hid_spk
  mkdir -p $dir/${name}_spk/log
  tmpdir=$dir/${name}_spk
  $cmd JOB=1:$nj $tmpdir/log/decode.JOB.log \
    nnet3-get-egs-dense-triple-targets-utt --compress=false --num-targets=129 \
      --left-context-1=$model_left_context_1 --right-context-1=$model_right_context_1 \
      --left-context-2=$model_left_context_2 --right-context-2=$model_right_context_2 \
      --left-context-3=$model_left_context_3 --right-context-3=$model_right_context_3 \
      "$feats_phone" "$feats_spk" "$feats_emo" "$feats_targets" ark:- \| \
    nnet3-compute-from-egs --use-gpu=no $dir/final.hid_spk ark:- ark,scp:$tmpdir/feats.JOB.ark,$tmpdir/feats.JOB.scp
  wait;
  for n in $(seq $nj); do
    cat $tmpdir/feats.$n.scp || exit 1;
  done > $tmpdir/feats.scp
  rm $dir/final.hid_spk
fi

if [ $stage -le 0 ]; then
  echo decoding emotion spectrum.
  nnet3-copy --binary=false $dir/final.raw $dir/final.hid_emotion
  sed -i "s/input=combine/input=final-affine-e/g" $dir/final.hid_emotion
  mkdir -p $dir/${name}_emotion/log
  tmpdir=$dir/${name}_emotion
  $cmd JOB=1:$nj $tmpdir/log/decode.JOB.log \
    nnet3-get-egs-dense-triple-targets-utt --compress=false --num-targets=129 \
      --left-context-1=$model_left_context_1 --right-context-1=$model_right_context_1 \
      --left-context-2=$model_left_context_2 --right-context-2=$model_right_context_2 \
      --left-context-3=$model_left_context_3 --right-context-3=$model_right_context_3 \
      "$feats_phone" "$feats_spk" "$feats_emo" "$feats_targets" ark:- \| \
    nnet3-compute-from-egs --use-gpu=no $dir/final.hid_emotion ark:- ark,scp:$tmpdir/feats.JOB.ark,$tmpdir/feats.JOB.scp
  wait;
  for n in $(seq $nj); do
    cat $tmpdir/feats.$n.scp || exit 1;
  done > $tmpdir/feats.scp
  rm $dir/final.hid_emotion
fi

echo "Created decoding "
