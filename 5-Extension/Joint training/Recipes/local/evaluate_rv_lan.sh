#!/bin/bash

# Copyright  2018  Lantian Li
# Apache 2.0.

# this recipe is used for i-vector evaluation on language recognition.

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# != 5 ]; then
  echo "Usage: $0 [opts] <dtrain_1> <dtrain_2> <dtest> <trials_1> <trials_2>"
  exit 1;
fi

dtrain_1=$1      # dir of training set on lan1
dtrain_2=$2      # dir of training set on lan2
dtest=$3         # dir of test set on lan1
trials_1=$4      # file of test trial on lan1-lan1
trials_2=$5      # file of test trial on lan2-lan1

# Check some files.
for f in $dtrain_1/rvector.scp $dtrain_2/rvector.scp $dtest/rvector.scp ; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

# Cosine-distance scoring
cat $trials_1 | awk '{print $1, $2}' | \
 ivector-compute-dot-products - \
  "ark:ivector-normalize-length scp:$dtrain_1/lan_rvector.scp ark:- |" \
  "ark:ivector-normalize-length scp:$dtest/lan_rvector.scp ark:- |" \
   foo_cosine_lan1

cat $trials_2 | awk '{print $1, $2}' | \
 ivector-compute-dot-products - \
  "ark:ivector-normalize-length scp:$dtrain_2/lan_rvector.scp ark:- |" \
  "ark:ivector-normalize-length scp:$dtest/lan_rvector.scp ark:- |" \
   foo_cosine_lan2