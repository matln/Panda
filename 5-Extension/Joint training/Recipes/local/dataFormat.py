#!/usr/bin/python

# Copyright  2018  Lantian Li
# Apache 2.0.

import sys

def fun(fin, fout):
	fin = open(fin,'r')
	lines = fin.readlines()
	fin.close()
	fout = open(fout, 'w')
	for line in lines:
		part = line.split()
		if part[0][0] == 'f' or part[0][0] == 'm':
			label = 1
		else:
			label = 0
		data = ' '.join(part[2:-1])
		fout.write(str(label) + ' '  + data + '\n')
	fout.close()
	
if __name__ == '__main__':
	fin = sys.argv[1]
	fout = sys.argv[2]
	fun(fin, fout)
