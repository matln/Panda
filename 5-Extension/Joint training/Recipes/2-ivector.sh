#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li
# this is the standard i-vector system.

. ./cmd.sh
. ./path.sh

set -e

stage=0
# number of components
cnum=1024
civ=200
clda=100

path=data/data_mfcc/CSLT-C200_WSJ-E200    # data-dir of the training set.
data=`basename $path`

# UBM/T-matrix training
if [ $stage -le 0 ]; then
  # subset for fast training.
  utils/subset_data_dir.sh $path 5000 $path/${data}_5k
  utils/subset_data_dir.sh $path 10000 $path/${data}_10k

  # diag-UBM training
  sid/train_diag_ubm.sh --nj 10 --cmd "$train_cmd" $path/${data}_5k $cnum \
    exp_ivector/$data/diag_ubm_${cnum}
  # full-UBM training
  sid/train_full_ubm.sh --nj 10 --cmd "$train_cmd" $path/${data}_10k \
    exp_ivector/$data/diag_ubm_${cnum} exp_ivector/$data/full_ubm_${cnum}
  # T-matrix
  sid/train_ivector_extractor.sh --nj 6 --cmd "$train_cmd -l mem_free=2G" \
    --num-iters 5 --ivector_dim $civ exp_ivector/$data/full_ubm_${cnum}/final.ubm $path \
    exp_ivector/$data/extractor_${cnum}_${civ}
fi


# LDA/PLDA training for speaker recognition
if [ $stage -le 1 ]; then
  # i-vector extraction on the training set.
  sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=2G" --nj 10 \
    exp_ivector/$data/extractor_${cnum}_${civ} $path \
    exp_ivector/$data/ivectors_${data}_${cnum}_${civ}
  
  # compute LDA transform
  ivector-compute-lda --dim=$clda  --total-covariance-factor=0.1 \
    "ark:ivector-normalize-length scp:exp_ivector/$data/ivectors_${data}_${cnum}_${civ}/ivector.scp ark:- |" \
    ark:$path/utt2spk \
    exp_ivector/${data}/ivectors_${data}_${cnum}_${civ}/transform_${clda}.mat

  # compute PLDA transform
  ivector-compute-plda ark:$path/spk2utt \
    "ark:ivector-normalize-length scp:exp_ivector/${data}/ivectors_${data}_${cnum}_${civ}/ivector.scp  ark:- |" \
    exp_ivector/$data/ivectors_${data}_${cnum}_${civ}/plda 2>exp_ivector/$data/ivectors_${data}_${cnum}_${civ}/log/plda.log
fi


# i-vector extraction on the evaluation set.
if [ $stage -le 2 ]; then
  path=data/data_mfcc/CSLT-C100
  for sub in model_C100 test_C100; do
    sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=2G" --nj 10 \
      exp_ivector/$data/extractor_${cnum}_${civ} $path/$sub \
      exp_ivector/$data/ivectors_${sub}_${cnum}_${civ}
  done

  path=data/data_mfcc/WSJ-E110
  for sub in model_E110 test_E110 ; do
    sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=2G" --nj 10 \
      exp_ivector/$data/extractor_${cnum}_${civ} $path/$sub \
      exp_ivector/$data/ivectors_${sub}_${cnum}_${civ}
  done
fi


# i-vector evaluation on speaker recognition
if [ $stage -le 3 ]; then
  echo china
  local/evaluate_iv_spk.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_model_C100_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_C100_${cnum}_${civ} \
    data/trials/speaker/C100.trl

  echo wsj
  local/evaluate_iv_spk.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_model_E110_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_E110_${cnum}_${civ} \
    data/trials/speaker/E110.trl
fi


# i-vector evaluation on language recognition with cosine metric
if [ $stage -le 4 ]; then
  echo china
  local/evaluate_iv_lan.sh \
    exp_ivector/$data/ivectors_model_C100_${cnum}_${civ} \
    exp_ivector/$data/ivectors_model_E110_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_C100_${cnum}_${civ} \
    data/trials/language/china_china.trl
    data/trials/language/wsj_china.trl
  
  paste foo_cosine_lan1 foo_cosine_lan2 | awk '{print $3, $6}' > foo_cosine_china
  rm foo_cosine_lan1 foo_cosine_lan2

  echo wsj
  local/evaluate_iv_lan.sh \
    exp_ivector/$data/ivectors_model_E110_${cnum}_${civ} \
    exp_ivector/$data/ivectors_model_C100_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_E110_${cnum}_${civ} \
    data/trials/language/wsj_wsj.trl
    data/trials/language/china_wsj.trl
  
  paste foo_cosine_lan1 foo_cosine_lan2 | awk '{print $3, $6}' > foo_cosine_wsj
  rm foo_cosine_lan1 foo_cosine_lan2
  # compute IDE and IDR.
  python local/genIDR.py foo_cosine_china foo_cosine_wsj
fi

# i-vector evaluation on language recognition with linear SVM
if [ $stage -le 5 ]; then
  path=exp_ivector/$data/svm
  mkdir -p $path/dev $path/test
  # dev
  ivector-normalize-length scp:exp_ivector/$data/ivectors_${data}_${cnum}_${civ}/ivector.scp ark,t:$path/dev/dev.ark
  python local/dataFormat.py $path/dev/dev.ark $path/dev/dev.dat
  # test
  ivector-normalize-length scp:exp_ivector/$data/ivectors_test_C100_${cnum}_${civ}/ivector.scp ark,t:$path/test/C100.ark
  python local/dataFormat.py $path/test/C100.ark $path/test/C100.dat
  ivector-normalize-length scp:exp_ivector/$data/ivectors_test_E110_${cnum}_${civ}/ivector.scp ark,t:$path/test/E110.ark
  python local/dataFormat.py $path/test/E110.ark $path/test/E110.dat
  cat $path/test/C100.dat $path/test/E110.dat > $path/test/test.dat
  # SVM training and evaluation.
  python steps/make_svm.py $path/dev/dev.dat $path/test/test.dat
fi

