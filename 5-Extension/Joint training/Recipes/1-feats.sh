#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li

. ./cmd.sh
. ./path.sh
stage=0

set -e
mfccdir=`pwd`/_mfcc
vaddir=`pwd`/_vad
fbankdir=`pwd`/_fbank


if [ $stage -le 0 ]; then
  # Feature extraction on the training set.
  data=data/data_mfcc/CSLT-C200_WSJ-E200
  # Make MFCCs.
  steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 20 --cmd "$train_cmd" \
    $data exp_ivector/_log_mfcc $mfccdir
  # Make energy-vad.
  sid/compute_vad_decision.sh --nj 10 --cmd "$train_cmd" \
    $data exp_ivector/_log_vad $vaddir
  
  data=data/data_fbank/CSLT-C200_WSJ-E200
  # Make Fbanks.
  steps/make_fbank.sh --fbank-config conf/fbank.conf --nj 20 --cmd "$train_cmd" \
    $data exp_rvector/_log_fbank $fbankdir
  # Make CMVN.
  steps/compute_cmvn_stats.sh $data $data/_cmvn $data/local/cmvn
  cp data/data_mfcc/CSLT-C200_WSJ-E200/vad.scp data/data_fbank/CSLT-C200_WSJ-E200/vad.scp
fi


if [ $stage -le 1 ]; then
  for data in CSLT-C100/model_C100 CSLT-C100/test_C100 WSJ-E110/model_E110 WSJ-E110/test_E110 ; do
    # Make MFCCs.
    steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 20 --cmd "$train_cmd" \
      data/data_mfcc/$data exp_ivector/_log_mfcc $mfccdir
    # Make energy-vad.
    sid/compute_vad_decision.sh --nj 10 --cmd "$train_cmd" \
      data/data_mfcc/$data exp_ivector/_log_vad $vaddir

    # Make Fbanks.
    steps/make_fbank.sh --fbank-config conf/fbank.conf --nj 20 --cmd "$train_cmd" \
      data/data_fbank/$data exp_rvector/_log_fbank $fbankdir
    # Make CMVN.
    steps/compute_cmvn_stats.sh data/data_fbank/$data data/data_fbank/$data/_cmvn data/data_fbank/$data/local/cmvn
    cp data/data_mfcc/$data/vad.scp data/data_fbank/$data/vad.scp
  done
fi

echo Done.