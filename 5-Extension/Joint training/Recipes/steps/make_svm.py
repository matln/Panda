#!/usr/bin/python

# Copyright  2015  Lantian Li
# Apache 2.0.

import os, sys, math
import scipy as sp
import numpy as np
from numpy import linalg
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import DistanceMetric

if __name__ == "__main__":

	fin_train = sys.argv[1]
	fin_test = sys.argv[2]
	sys.stderr.write('Paras: ' + fin_train + ' ' + fin_test + ' ' + '\n')

	# data preparation for training set
	fin = open(fin_train, 'r')
	train_data = []
	train_label = []
	for line in fin:
		line = line.strip()
		wordList = line.split()
		tempList = []
		tempList = np.array(wordList[1:], dtype=float)
                train_data.append(tempList)
                train_label.append(np.array(wordList[0], dtype=int))
	sys.stderr.write('train_data: ' + str(len(train_data)) + ' ' + 'train_label: ' + str(len(train_label)) + '\n')
	fin.close()

	# model training
	clf = svm.SVC(kernel = 'linear', probability = True, random_state = 777, class_weight = 'balanced')
	clf.fit(train_data, train_label)
	sys.stderr.write('Training Done!' + '\n')

	# data preparation for test test
	test_data = []
	test_label = []
	fin = open(fin_test, 'r')
	for line in fin:
		line = line.strip()
		wordList = line.split()
		tempList = []
		tempList = np.array(wordList[1:], dtype=float)
                test_data.append(tempList)
                test_label.append(np.array(wordList[0], dtype=int))
	sys.stderr.write('test_data: ' + str(len(test_data)) + ' ' + 'test_label: ' + str(len(test_label)) + '\n')
	fin.close()	

	# predict
	correct = 0
	incorrect = 0
	for i in range(len(test_data)):
		pre = clf.predict(test_data[i].reshape(1,  -1))
		if pre[0] == test_label[i]:
			correct += 1
		else:
			incorrect += 1
	sys.stderr.write('IDE: ' + str(incorrect) + '\n')
	sys.stderr.write('IDR: ' + str(round(100.0*incorrect / len(test_data), 2)) + '%\n')