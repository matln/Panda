# "queue.pl" uses qsub.  The options to it are
# options to qsub.  If you have GridEngine installed,
# change this to a queue you have access to.
# Otherwise, use "run.pl", which will run jobs locally
# (make sure your --num-jobs options are no more than
# the number of cpus on your machine.

#a) JHU cluster options
export cmd="run.pl"
# decoding
export train_cmd="queue.pl -l arch=*64* -q wolf.cpu.q"
# network training
export gpu_cmd="queue.pl -q wolf.gpu.q"