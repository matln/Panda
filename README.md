A brief introduction of this folder.

1-Basic: Basic CT-DNN model for deep speaker feature learning.

2-Generalization: Research on the generalizability of deep speaker features.

3-Full-Info: Full-info training for deep speaker feature learning.

4-Phone-Aware: Phone-aware training for deep speaker feature learning.

5-Extension: Extended research including joint training and deep factorization.

6-Thesis: Materials of thesis.

More details please refer to the related folder and READMEs.

Ps: All the models and related data can be found from my home-dir on the server (/work8/lilt/My_Phd_Work).

gitlab: git@gitlab.com:csltstu/Panda.git
        https://gitlab.com/csltstu/Panda.git

Author by Lantian Li (lilt@mail.tsinghua.edu.cn), Sept. 10. 2018
