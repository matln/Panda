#!/bin/bash
# Copyright   2018  Tsinghua University (Author: Lantian Li)
# Apache 2.0.

# this recipe is used for phonetic feature extraction.

nj=6
cmd="run.pl"
stage=-2

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# != 3 ]; then
  echo "Usage: $0 [opts] <nnet3-dir> <data-dir> <dve-dir>"
  echo "  --nj <nj>   # number of parallel jobs"
  echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs."
  exit 1;
fi

dir=$1
data=$2
dedir=$3

# check data.
for f in $data/feats.scp $data/utt2spk $data/cmvn.scp ; do
  [ ! -f $f ] && echo "No such file $f" && exit 1;
done

sdata=$data/split$nj 
utils/split_data.sh $data $nj

# check nnet.
if [ $stage -le -2 ]; then
  if [ ! -f $dir/final.hid_out ]; then
  	nnet3-copy --binary=false $dir/final.raw $dir/final.hid_out
    sed -i "s/input=final-log-softmax/input=final-svd_v/g" $dir/final.hid_out
  fi
fi

# phonetic feature extraction.
if [ $stage -le -1 ]; then
  mkdir -p $dedir/log
  cmvn_opts=$(cat $dir/cmvn_opts)
  echo $cmvn_opts
  feats="ark:copy-feats scp:$sdata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp ark:- ark:- |"

  echo $nj
  $cmd JOB=1:$nj $dedir/log/decode.JOB.log \
    nnet3-compute --use-gpu=no $dir/final.hid_out "$feats" ark,scp:$dedir/feats.JOB.ark,$dedir/feats.JOB.scp
  wait;

  for n in $(seq $nj); do
    cat $dedir/feats.$n.scp || exit 1;
  done > $dedir/feats.scp
fi

echo "Created decoding."
