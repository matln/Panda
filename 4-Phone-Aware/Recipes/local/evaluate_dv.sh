#!/bin/bash

# Copyright  2018  Lantian Li
# Apache 2.0.

# this recipe is used for d-vector evaluation.

lda=150
cleanup=true

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# != 4 ]; then
  echo "Usage: $0 [opts] <trials> <ddev> <dtrain> <dtest>"
  echo "  --lda  # dim of lda-vector."
  echo "  --cleanup  # clean up foo or not."
  exit 1;
fi

ddev=$1        # dir of dev. set
dtrain=$2      # dir of training set
dtest=$3       # dir of test set 
trials=$4      # file of test trial

echo $dtrain $dtest

# Check some files.
for f in $ddev/transform_${clda}.mat $ddev/plda $dtrain/dvector.scp $dtest/dvector.scp ; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

# Cosine-distance scoring
trials=$trials
cat $trials | awk '{print $1, $2}' | \
 ivector-compute-dot-products - \
  "ark:ivector-normalize-length scp:$dtrain/spk_dvector.scp ark:- |" \
  "ark:ivector-normalize-length scp:$dtest/spk_dvector.scp ark:- |" \
   $ddev/foo_cosine
local/score.sh $trials $ddev/foo_cosine

# Demonstrate what happens if we reduce the dimension with LDA
trials=$trials
cat $trials | awk '{print $1, $2}' | \
 ivector-compute-dot-products - \
  "ark:ivector-normalize-length scp:$dtrain/spk_dvector.scp ark:- | ivector-transform $ddev/transform_${clda}.mat ark:- ark:- | ivector-normalize-length ark:- ark:- |" \
  "ark:ivector-normalize-length scp:$dtest/spk_dvector.scp ark:- | ivector-transform $ddev/transform_${clda}.mat ark:- ark:- | ivector-normalize-length ark:- ark:- |" \
  $ddev/foo_lda
local/score.sh $trials $ddev/foo_lda

# Demonstrate PLDA scoring
ivector-plda-scoring --simple-length-normalization=true --num-utts=ark:$dtrain/num_utts.ark \
   "ivector-copy-plda --smoothing=0.0 $ddev/plda - |" \
   "ark:ivector-normalize-length scp:$dtrain/spk_dvector.scp ark:- | ivector-subtract-global-mean ark:- ark:- |" \
   "ark:ivector-normalize-length scp:$dtest/spk_dvector.scp ark:- | ivector-subtract-global-mean ark:- ark:- |" \
   "cat '$trials' | awk '{print \$1, \$2}' |" $ddev/foo_plda
local/score.sh $trials $ddev/foo_plda

$cleanup && rm $ddev/foo_*