#!/bin/bash
# Copyright 2013   Daniel Povey
#           2018   Lantian Li
# this is the d-vector system with phone-aware training(PAT CT-DNN model), built in nnet3.

stage=0
train_stage=-10

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

data=fisher_5000   # name of the training set, given fisher_5000 as an example.
pnorm_input=2000   # dim of pnorm input.
pnorm_output=400   # dim of pnorm output.
dir=exp_ctdnn/$data/nnet3/cnn_4_8_pi${pnorm_input}_po${pnorm_output}_l6_splice10_phone   # dir of PAT CT-DNN models.
train_data=data/data_fbank/$data  # data-dir of training set.
phone_data=phone/$data    # phonetic data-dir of training set.

# prepare alignment
if [ $stage -le 0 ]; then
  # generate utt2spk_num
  python local/spk_num.py $train_data
  # generate feats.len
  feat-to-len scp:$train_data/feats.scp ark,t:$train_data/feats.len
  # generate vad.ark
  copy-vector scp:$train_data/vad.scp ark,t:$train_data/vad.ark
  # generate ali.ark, spk_counts and spk_num
  mkdir -p exp_ctdnn/$data/spk_ali
  python local/spk_ali.py $train_data exp_ctdnn/$data/spk_ali
fi


# PAT CT-DNN model training
if [ $stage -le 1 ]; then
  steps/nnet3/train_cnn_spk_vad_phone.sh --stage $train_stage \
    --num-epochs 15 --num-jobs-initial 2 --num-jobs-final 8 \
    --splice-indexes "-4,-3,-2,-1,0,1,2,3,4  0  -2,2  0  -4,4  0" \
    --feat-type raw \
    --cmvn-opts "--norm-means=false --norm-vars=false" \
    --cmvn-opts-p "--norm-means=false --norm-vars=false" \      # configurable "--norm-means=true --norm-vars=false"
    --initial-effective-lrate 0.005 --final-effective-lrate 0.0005 \
    --cmd "$train_cmd" \
    --gpu-cmd "$gpu_cmd" \
    --pnorm-input-dim $pnorm_input \
    --pnorm-output-dim $pnorm_output \
    --remove-egs true \
    $train_data $phone_data exp_ctdnn/$data/spk_ali $dir || exit 1;
fi


# LDA/PLDA training
if [ $stage -le 2 ]; then
  # d-vector extraction on the training set.
  sid/extract_phone_dvectors.sh --nj 20 --cmd "$train_cmd" $dir $train_data $phone_data $dir/$data

  # compute LDA transform
  ivector-compute-lda --dim=$clda  --total-covariance-factor=0.1 \
    "ark:ivector-normalize-length scp:$dir/$data/dvector.scp ark:- |" \
    ark:$train_data/utt2spk \
    $dir/$data/transform_${clda}.mat

  # compute PLDA transform
  ivector-compute-plda ark:$train_data/spk2utt \
    "ark:ivector-normalize-length scp:$dir/$data/dvector.scp ark:- |" \
    $dir/$data/plda 2>$dir/$data/log/plda.log
fi


# d-vector extraction on the evaluation set.
if [ $stage -le 3 ]; then
  lang=data/data_fbank/chinese
  for sub in train-ch test-ch ; do
    sid/extract_phone_dvectors.sh --nj 20 --cmd "$train_cmd" $dir $lang/$sub phone/chinese/$sub $dir/$sub
  done

  lang=data/data_fbank/uyghur
  for sub in train-uy test-uy ; do
    sid/extract_phone_dvectors.sh --nj 20 --cmd "$train_cmd" $dir $lang/$sub phone/uyghur/$sub $dir/$sub
  done
fi


# d-vector evaluation
### long trials
if [ $stage -le 4 ]; then
  echo Long-Ch-Ch
  local/evaluate_dv.sh $dir/$data \
    $dir/train-ch \
    $dir/test-ch \
    data/trials/long/ch-ch.trials

  echo Long-Uy-Uy
  local/evaluate_dv.sh $dir/$data \
    $dir/train-uy \
    $dir/test-uy \
    data/trials/long/uy-uy.trials

  echo Long-Ch-Uy
  local/evaluate_dv.sh $dir/$data \
    $dir/train-ch \
    $dir/test-uy \
    data/trials/long/ch-uy.trials

  echo Long-Uy-Ch
  local/evaluate_dv.sh $dir/$data \
    $dir/train-uy \
    $dir/test-ch \
    data/trials/long/uy-ch.trials
fi

### short trials
if [ $stage -le 5 ]; then
  echo Short-Ch-Ch
  local/evaluate_dv.sh $dir/$data \
    $dir/test-ch \
    $dir/test-ch \
    data/trials/short/ch-ch.trials

  echo Short-Uy-Uy
  local/evaluate_dv.sh $dir/$data \
    $dir/test-uy \
    $dir/test-uy \
    data/trials/short/uy-uy.trials

  echo Short-Ch-Uy
  local/evaluate_dv.sh $dir/$data \
    $dir/test-ch \
    $dir/test-uy \
    data/trials/short/ch-uy.trials
fi

echo Done.
