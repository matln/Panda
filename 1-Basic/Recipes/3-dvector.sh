#!/bin/bash
# Copyright 2013   Daniel Povey
#           2018   Lantian Li
# this is the standard d-vector system, built in nnet3; it's what we use to embed deep speaker vector.

stage=0
train_stage=-10

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

data=fisher_5000   # name of the training set, given fisher_5000 as an example.
pnorm_input=2000   # dim of pnorm input.
pnorm_output=400   # dim of pnorm output.
dir=exp_ctdnn/$data/nnet3/cnn_4_8_pi${pnorm_input}_po${pnorm_output}_l6_splice10   # dir of CT-DNN models.
train_data=data/data_fbank/fisher_train/$data  # data-dir of training set.

# prepare alignment
if [ $stage -le 0 ]; then
  # generate utt2spk_num
  python local/spk_num.py $train_data
  # generate feats.len
  feat-to-len scp:$train_data/feats.scp ark,t:$train_data/feats.len
  # generate vad.ark
  copy-vector scp:$train_data/vad.scp ark,t:$train_data/vad.ark
  # generate ali.ark, spk_counts and spk_num
  mkdir -p exp_ctdnn/$data/spk_ali
  python local/spk_ali.py $train_data exp_ctdnn/$data/spk_ali
fi


# CT-DNN model training
if [ $stage -le 1 ]; then
  steps/nnet3/train_cnn_spk_vad.sh --stage $train_stage \
    --num-epochs 15 --num-jobs-initial 2 --num-jobs-final 8 \
    --splice-indexes "-4,-3,-2,-1,0,1,2,3,4  0  -2,2  0  -4,4  0" \
    --feat-type raw \
    --cmvn-opts "--norm-means=false --norm-vars=false" \
    --initial-effective-lrate 0.005 --final-effective-lrate 0.0005 \
    --cmd "$train_cmd" \
    --gpu-cmd "$gpu_cmd" \
    --pnorm-input-dim $pnorm_input \
    --pnorm-output-dim $pnorm_output \
    --remove-egs true \
    $train_data exp_ctdnn/$data/spk_ali $dir || exit 1;
fi


# LDA/PLDA training
if [ $stage -le 2 ]; then
  # d-vector extraction on the training set.
  sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir $train_data $dir/$data

  # compute LDA transform
  ivector-compute-lda --dim=$clda  --total-covariance-factor=0.1 \
    "ark:ivector-normalize-length scp:$dir/$data/dvector.scp ark:- |" \
    ark:$train_data/utt2spk \
    $dir/$data/transform_${clda}.mat

  # compute PLDA transform
  ivector-compute-plda ark:$train_data/spk2utt \
    "ark:ivector-normalize-length scp:$dir/$data/dvector.scp ark:- |" \
    $dir/$data/plda 2>$dir/$data/log/plda.log
fi


# d-vector extraction on the evaluation set.
if [ $stage -le 3 ]; then
  path=data/data_fbank/fisher_test
  for eva in train_all test_3 test_9 test_18 test_30; do
    sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir $path/$eva $dir/$eva
  done

  for eva in test_short_20 test_short_50 test_short_100; do
    sid/extract_dvectors_seg.sh --nj 20 --cmd "$train_cmd" $dir $path/$eva $dir/$eva
  done
fi


# d-vector evaluation
if [ $stage -le 4 ]; then
  echo 30-20f
  local/evaluate_dv.sh $dir/$data \
    $dir/train_all \
    $dir/test_short_20 \
    data/trials/30-20f.trl

  echo 30-50f
  local/evaluate_dv.sh $dir/$data \
    $dir/train_all \
    $dir/test_short_50 \
    data/trials/30-50f.trl

  echo 30-100f
  local/evaluate_dv.sh $dir/$data \
    $dir/train_all \
    $dir/test_short_100 \
    data/trials/30-100f.trl

  echo 30-3
  local/evaluate_dv.sh $dir/$data \
    $dir/train_all \
    $dir/test_3 \
    data/trials/30-3.trl

  echo 30-9
  local/evaluate_dv.sh $dir/$data \
    $dir/train_all \
    $dir/test_9 \
    data/trials/30-9.trl

  echo 30-18
  local/evaluate_dv.sh $dir/$data \
    $dir/train_all \
    $dir/test_18 \
    data/trials/30-18.trl

  echo 30-30
  local/evaluate_dv.sh $dir/$data \
    $dir/train_all \
    $dir/test_30 \
    data/trials/30-30.trl
fi

echo Done.
