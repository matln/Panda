#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li

. ./cmd.sh
. ./path.sh
stage=0

set -e
mfccdir=`pwd`/_mfcc
vaddir=`pwd`/_vad
fbankdir=`pwd`/_fbank

if [ $stage -le 0 ]; then
  # Feature extraction on the training set, given fisher_5000 as an example.
  data=data/data_mfcc/fisher_train/fisher_5000
  # Make MFCCs.
  steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 20 --cmd "$train_cmd" \
    $data exp_ivector/_log_mfcc $mfccdir
  # Make energy-vad.
  sid/compute_vad_decision.sh --nj 10 --cmd "$train_cmd" \
    $data exp_ivector/_log_vad $vaddir
  
  data=data/data_fbank/fisher_train/fisher_5000
  # Make Fbanks.
  steps/make_fbank.sh --fbank-config conf/fbank.conf --nj 20 --cmd "$train_cmd" \
    $data exp_ctdnn/_log_fbank $fbankdir
  # Make CMVN.
  steps/compute_cmvn_stats.sh $data $data/_cmvn $data/local/cmvn

  cp data/data_mfcc/fisher_train/fisher_5000/vad.scp data/data_fbank/fisher_train/fisher_5000/vad.scp

fi

if [ $stage -le 1 ]; then
  # Feature extraction on the evaluation set.
  data=data/data_mfcc/fisher_test/fisher_1000
  # Make MFCCs.
  steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 20 --cmd "$train_cmd" \
    $data exp_ivector/_log_mfcc $mfccdir
  # Make energy-vad.
  sid/compute_vad_decision.sh --nj 10 --cmd "$train_cmd" \
    $data exp_ivector/_log_vad $vaddir

  data=data/data_fbank/fisher_test/fisher_1000  
  # Make Fbanks.
  steps/make_fbank.sh --fbank-config conf/fbank.conf --nj 20 --cmd "$train_cmd" \
    $data exp_ctdnn/_log_fbank $fbankdir
  # Make CMVN.
  steps/compute_cmvn_stats.sh $data $data/_cmvn $data/local/cmvn

  cp data/data_mfcc/fisher_test/fisher_1000/vad.scp data/data_fbank/fisher_test/fisher_1000/vad.scp
fi

echo Done.